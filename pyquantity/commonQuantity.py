"""Module with common quantities"""
from . import Quantity


class Time(Quantity):
	pass


class Lenght(Quantity):
	pass


class Mass(Quantity):
	pass


class Velocity(Quantity):
	pass


class Temperature(Quantity):
	pass
